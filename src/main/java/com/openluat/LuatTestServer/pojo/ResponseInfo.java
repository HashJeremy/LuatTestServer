package com.openluat.LuatTestServer.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseInfo {
    private int code;
    private String message;
    private Date timeStamp;
}
