package com.openluat.LuatTestServer.pojo;

import lombok.Data;

@Data
public class LbsLocInfo {
    private int deviceID;
    private double lat;
    private double lng;
    private int timestamp;
}
