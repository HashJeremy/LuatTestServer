package com.openluat.LuatTestServer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Objects;

@ConfigurationProperties(prefix = "my.config")
public class TestConfig {
    private Boolean socketServerEnable;

    public Boolean getSocketServerEnable() {
        return socketServerEnable;
    }

    public void setSocketServerEnable(Boolean socketServerEnable) {
        this.socketServerEnable = socketServerEnable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestConfig that = (TestConfig) o;

        return Objects.equals(socketServerEnable, that.socketServerEnable);
    }

    @Override
    public int hashCode() {
        return socketServerEnable != null ? socketServerEnable.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TestConfig{" +
                "socketServerEnable=" + socketServerEnable +
                '}';
    }
}
